'use strict';

var UserController = require("./api/user/user.controller");
var PostController = require("./api/post/post.controller");
var AWSController = require("./api/aws/aws.controller");
var express = require("express");
var config = require("./config");

const API_POSTS_URI = "/api/posts";
const API_USERS_URI = "/api/users";
const API_AWS_URI = "/api/aws";
const HOME_PAGE = "/home.html#!/mainpage";
const SIGNIN_PAGE = "/home.html#!/signIn";


module.exports = function (app, passport) {
    // Posts API
    app.get(API_POSTS_URI, isAuthenticated, PostController.list);
    app.get(API_POSTS_URI + '/image/:url', isAuthenticated, PostController.showImage);
    app.get(API_POSTS_URI + '/me', isAuthenticated, PostController.me);
    app.post(API_POSTS_URI, isAuthenticated, PostController.create);
    app.get(API_POSTS_URI + '/:id', isAuthenticated, PostController.get);
    app.post(API_POSTS_URI + '/:id', isAuthenticated, PostController.update);
    app.delete(API_POSTS_URI + '/:id', isAuthenticated, PostController.remove);
    app.post(API_POSTS_URI + '/:id/like', isAuthenticated, PostController.likePost);
    

    // Users API
    app.get(API_USERS_URI, isAuthenticated, UserController.list);
    app.post(API_USERS_URI, isAuthenticated, UserController.create);
    app.get(API_USERS_URI+ '/:id', isAuthenticated, UserController.get);
    app.get(API_USERS_URI + '/:id/posts', isAuthenticated, PostController.listByUser);
    app.post(API_USERS_URI + '/:id', isAuthenticated, UserController.update);
    app.delete(API_USERS_URI + '/:id', isAuthenticated, UserController.remove);
    app.get("/api/user/view-profile", isAuthenticated, UserController.profile);
    app.get("/api/user/social/profiles", isAuthenticated, UserController.profiles);

    
    app.get("/protected/", isAuthenticated, function(req, res){
        if(req.user == null){
            res.redirect(SIGNIN_PAGE);
        }
    })

    // AWS policy API
    app.get(API_AWS_URI + '/createS3Policy', isAuthenticated, PostController.list);
    app.post(API_AWS_URI + '/s3-policy', isAuthenticated, AWSController.getSignedPolicy);

    app.use(express.static(__dirname + "/../client/"));

    app.post("/change-password", isAuthenticated, UserController.changePasswd);

    app.get("/api/user/get-profile-token", UserController.profileToken);
    app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    app.post('/register', UserController.register);

    app.post("/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        failureRedirect: "/",
        failureFlash : true
    }));

    app.post("/reset-password", UserController.resetPasswd);

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('..' + HOME_PAGE);
    });


    app.get("/oauth/google", passport.authenticate("google", {
        scope: ["email", "profile"]
    }));

    app.get("/oauth/google/callback", passport.authenticate("google", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE
    }));

    app.get("/oauth/facebook", passport.authenticate("facebook", {
        scope: ["email", "public_profile"]
    }));

    app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
        successRedirect: HOME_PAGE,
        failureRedirect: SIGNIN_PAGE,
        failureFlash : true
    }));

    app.get("/status/user", function (req, res) {
        var status = "";
        if(req.user) {
            status = req.user.email;
        }
        console.info("status of the user --> " + status);
        res.send(status).end();
    });

    app.get("/logout", function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });


    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect(SIGNIN_PAGE);
    }

    app.use(function(req, res, next){
        if(req.user == null){
            res.redirect(SIGNIN_PAGE);
        }
        next();
    });

};
