/**
 * Created by phangty on 10/11/16.
 */
var Local = require("passport-local").Strategy;
var Google = require("passport-google-oauth").OAuth2Strategy;
var Facebook = require("passport-facebook").Strategy;

var bcrypt   = require('bcryptjs');

var User = require("./database").User;
var AuthProvider = require("./database").AuthProvider;
var config = require("./config");

//Setup local 
module.exports = function (app, passport) {
    function authenticate(username, password, done) {

        User.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if(!result){
                return done(null, false);
            }else{
                if(bcrypt.compareSync(password , result.password)){
                    var whereClause = {};
                    whereClause.email = username;
                    User
                    .update({ reset_password_token: null},
                    {where:  whereClause});
                    return done(null, result);
                }else{
                    return done(null, false);
                }
            }
        }).catch(function(err){
            return done(err, false);
        });


    }

    function verifyCallback(accessToken, refreshToken, profile, done) {
        console.log(profile);
        if(profile.provider === 'google' || profile.provider === 'facebook'){
            id = profile.id;
            email = profile.emails[0].value;
            displayName = profile.displayName;
            provider_type = profile.provider;
            User.findOrCreate({where: {email: email}, defaults: {username: email , email: email, password: null, name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else if(profile.provider === 'twitter'){
            id = profile.id;
            twitterUsername = profile.username;
            displayName = profile.displayName;
            provider_type = profile.provider;
            User.findOrCreate({where: {email: twitterUsername}, defaults: {username: twitterUsername, email: twitterUsername, password: null , name: displayName}})
                .spread(function(user, created) {
                    console.log(user.get({
                        plain: true
                    }));
                    console.log(created);
                    AuthProvider.findOrCreate({where: {userid: user.id, providerType: provider_type},
                        defaults: {providerId: id, userId: user.id, providerType: provider_type, displayName: displayName}})
                        .spread(function(provider, created) {
                            console.log(provider.get({
                                plain: true
                            }));
                            console.log(created);
                        });
                    done(null, user);
                });
        }else{
            done(null, false);
        }
    }

    
    passport.use(new Google({
        clientID: config.GooglePlus_key,
        clientSecret: config.GooglePlus_secret,
        callbackURL: config.GooglePlus_callback_url
    }, verifyCallback))

    passport.use(new Facebook({
        clientID: config.Facebook_key,
        clientSecret: config.Facebook_secret,
        callbackURL: config.Facebook_callback_url,
        profileFields: ['id', 'displayName', 'photos', 'email']
    }, verifyCallback))

    passport.use(new Local({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        User.findOne({
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, user);
            }
        }).catch(function(err){
            done(err, user);
        });
    });

};
