(function () {
    angular
        .module("projectApp")
        .config(projectAppConfig);
    projectAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function projectAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login.html"
                    }
                },
                controller: 'LoginCtrl',
                controllerAs: 'ctrl'
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html"
                    }
                },
                controller: 'RegisterCtrl',
                controllerAs: 'ctrl'
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html"
                    }
                },
                controller: 'ResetPasswordCtrl',
                controllerAs: 'ctrl'
            })
            ////////////////////////////////////////////////////////////////////////////
            .state("ChangeNewpassword", {
                url: "/ChangeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html"
                    }
                },
                controller: 'ChangeNewPasswordCtrl',
                controllerAs: 'ctrl'
            })
            ////////////////////////////////////////////////////////////////////////////
            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'MyAccountCtrl',
                controllerAs: 'ctrl'
            })
            .state("ChangePassword", {
                url: "/ChangePassword",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/changePassword.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'ChangePasswordCtrl',
                controllerAs: 'ctrl'
            })
            .state('mainpage', {
                url: '/mainpage',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/mainpage.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('aboutus', {
                url: '/aboutus',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/aboutus.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'AboutUsCtrl',
                controllerAs: 'ctrl'
            })
            .state('GGG', {
                url: '/GGG',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/GGG.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'GGGListCtrl',
                controllerAs: 'ctrl'
            })
            .state('SSS', {
                url: '/SSS',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/SSS.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'SSSCtrl',
                controllerAs: 'ctrl'
            })
            .state('profile', {
                url: '/profile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html"
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            .state('back', {
                url: '/back',
                templateUrl: './app/users/login.html',
                controller: 'PostListCtrl',
                controllerAs: 'ctrl'
            })
            
        $urlRouterProvider.otherwise("/signIn");


    }
})();
