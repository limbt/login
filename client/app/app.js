(function () {
    angular
        .module("projectApp", [
            "ngFileUpload",
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "ngProgress",
            "ngMessages",
            "data-table"
        ]);
})();